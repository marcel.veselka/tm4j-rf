# Inspiration
# https://robotframework.org/robotframework/latest/RobotFrameworkUserGuide.html#listener-version-3
# https://github.com/reportportal/agent-Python-RobotFramework/blob/master/robotframework_reportportal/listener.py

# tm4j docs
#
# https://support.smartbear.com/tm4j-cloud/docs/api-and-test-automation/generating-access-keys.html
# https://support.smartbear.com/tm4j-cloud/api-docs/

import logging

from .variables import Variables
from .model import Test, Suite, LogMessage
from .service import RobotService

ROBOT_LISTENER_API_VERSION = 3

items = []


def start_launch(launch):
    """Start a new launch."""
    if not Variables.launch_id:
        launch.doc = Variables.launch_doc
        logging.debug("Tesena TM4J Listener - Start Launch: {0}".format(
            launch.attributes))
        RobotService.start_launch(launch_name=Variables.launch_name,
                                  launch=launch)
    else:
        RobotService.rp.launch_id = Variables.launch_id


# Called when a test suite starts.
# data and result are model objects representing the executed test suite and its execution results, respectively.
def start_suite(name, attributes):
    suite = Suite(attributes=attributes)
    if suite.robot_id == "s1":
        Variables.check_variables()
        RobotService.init_service(Variables.endpoint, Variables.project,
                                  Variables.uuid)
        start_launch(suite)
        if not suite.suites:
            attributes['id'] = "s1-s1"
            start_suite(name, attributes)

    else:
        logging.debug("Tesena TM4J Listener - Start Suite: {0}".format(attributes))
        parent_id = items[-1][0] if items else None
        item_id = RobotService.start_suite(name=name, suite=suite,
                                           parent_item_id=parent_id)
        items.append((item_id, parent_id))


# Called when a test suite ends.
# Same arguments as with start_suite.
def end_suite(_, attributes):
    suite = Suite(attributes=attributes)
    if suite.robot_id == "s1":
        logging.debug(msg="Tesena TM4J Listener - End Launch: {0}".format(attributes))
        RobotService.finish_launch(launch=suite)
        RobotService.terminate_service()
    else:
        logging.debug("Tesena TM4J Listener - End Suite: {0}".format(attributes))
        RobotService.finish_suite(item_id=items.pop()[0], suite=suite)


# Called when a test case starts.
# data and result are model objects representing the executed test case and its execution results, respectively.
def start_test(name, attributes):
    test = Test(name=name, attributes=attributes)
    logging.debug("Tesena TM4J Listener - Start Test: {0}".format(attributes))
    parent_item_id = items[-1][0]
    items.append((
        RobotService.start_test(test=test, parent_item_id=parent_item_id),
        parent_item_id))


# Called when a test case ends.
# Same arguments as with start_test.
def end_test(name, attributes):
    test = Test(name=name, attributes=attributes)
    item_id, _ = items.pop()
    logging.debug("Tesena TM4J Listener - End Test: {0}".format(attributes))
    RobotService.finish_test(item_id=item_id, test=test)


# Called when an executed keyword writes a log message. message is a model object representing the logged message.
# This method is not called if the message has level below the current threshold level.
def log_message(message):
    # Check if message comes from our custom logger or not
    if isinstance(message["message"], LogMessage):
        msg = message["message"]
    else:
        msg = LogMessage(message["message"])
        msg.level = message["level"]

    msg.item_id = items[-1][0]
    logging.debug("Tesena TM4J Listener - Log Message: {0}".format(message))
    RobotService.log(message=msg)


# Called when the framework itself writes a syslog message.
# message is same object as with log_message.
def message(message):
    logging.debug("Tesena TM4J Listener - Message: {0}".format(message))


# Called when writing to an output file is ready.
# path is an absolute path to the file.
def output_file(path):
    logging.debug("Tesena TM4J Listener - Output File: {0}".format(path))


# Called when writing to a log file is ready.
# path is an absolute path to the file.
def log_file(path):
    logging.debug("Tesena TM4J Listener - Log File: {0}".format(path))


# Called when writing to a report file is ready.
# path is an absolute path to the file.
def report_file(path):
    logging.debug("Tesena TM4J Listener - Report File: {0}".format(path))


# Called when writing to an xunit file is ready.
# path is an absolute path to the file.
def xunit_file(path):
    logging.debug("Tesena TM4J Listener - XUnit File: {0}".format(path))


# Called when writing to a debug file is ready.
# path is an absolute path to the file.
def debug_file(path):
    logging.debug("Tesena TM4J Listener - Debug File: {0}".format(path))


# Called when the whole test execution ends.
# With library listeners called when the library goes out of scope.
def close():
    logging.debug("Tesena TM4J Listener - Close")
