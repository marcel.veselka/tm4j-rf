from six import text_type


class Suite(object):
    def __init__(self, attributes):
        super(Suite, self).__init__()
        self.attributes = attributes
        self.suites = attributes["suites"]
        self.tests = attributes["tests"]
        self.doc = attributes["doc"]
        self.source = attributes["source"]
        self.total_tests = attributes["totaltests"]
        self.longname = attributes["longname"]
        self.robot_id = attributes["id"]
        self.metadata = attributes["metadata"]
        self.status = None
        self.message = None
        self.statistics = None
        if "status" in attributes.keys():
            self.status = attributes["status"]
        if "message" in attributes.keys():
            self.message = attributes["message"]
        if "statistics" in attributes.keys():
            self.statistics = attributes["statistics"]


class Test(object):
    def __init__(self, name=None, attributes=None):
        super(Test, self).__init__()
        self.name = name
        self.critical = attributes["critical"]
        self.template = attributes["template"]
        self.tags = attributes["tags"]
        self.doc = attributes["doc"]
        self.longname = attributes["longname"]
        self.robot_id = attributes["id"]
        self.status = None
        self.message = None
        if "status" in attributes.keys():
            self.status = attributes["status"]
        if "message" in attributes.keys():
            self.message = attributes["message"]


class LogMessage(text_type):
    def __init__(self, *args, **kwargs):
        super(LogMessage, self).__init__()
        self.item_id = None
        self.message = self
        self.level = "INFO"
        self.attachment = None
