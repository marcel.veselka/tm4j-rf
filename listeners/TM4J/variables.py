from robot.libraries.BuiltIn import BuiltIn

from .exception import RobotServiceException


def get_variable(name, default=None):
    return BuiltIn().get_variable_value("${" + name + "}", default=default)


class Variables(object):
    uuid = None
    endpoint = None
    launch_name = None
    project = None

    @staticmethod
    def check_variables():
        Variables.uuid = get_variable("TM4J_UUID", default=None)
        if Variables.uuid is None:
            raise RobotServiceException(
                "Missing parameter TM4J_UUID for robot run\n"
                "You should pass -v TM4J_UUID:<uuid_value>")
        Variables.endpoint = get_variable("TM4J_ENDPOINT", default=None)
        if Variables.endpoint is None:
            raise RobotServiceException(
                "Missing parameter TM4J_ENDPOINT for robot run\n"
                "You should pass -v TM4J_ENDPOINT:<endpoint_value>")
        Variables.launch_name = get_variable("TM4J_LAUNCH", default=None)
        if Variables.launch_name is None:
            raise RobotServiceException(
                "Missing parameter TM4J_LAUNCH for robot run\n"
                "You should pass -v TM4J_LAUNCH:<launch_name_value>")
        Variables.project = get_variable("TM4J_PROJECT", default=None)
        if Variables.project is None:
            raise RobotServiceException(
                "Missing parameter TM4J_PROJECT for robot run\n"
                "You should pass -v TM4J_PROJECT:<project_name_value>")
