import requests
from robot.libraries.BuiltIn import BuiltIn

ROBOT_LISTENER_API_VERSION = 3

def end_test(data, result):
    url = BuiltIn().get_variable_value("${ENDPOINT}")
    token = BuiltIn().get_variable_value("${TOKEN}")
    project_key= BuiltIn().get_variable_value("${PROJECT_KEY}")
    test_cycle_key = BuiltIn().get_variable_value("${TEST_CYCLE_KEY}")

    headers = {'Content-Type': 'application/json','Authorization': token}
    data = {'projectKey': project_key,'testCaseKey': result.tags[0],'testCycleKey': test_cycle_key,'statusName': result.status}

    response=requests.post(url,headers=headers, json=data)
    if response.status_code==201:
        print("Uspesne zapsano do JIRA")
    else:
        print(response.text)

