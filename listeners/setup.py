from setuptools import setup, find_packages


__version__ = '0.0.1'

requirements = [
    "six",
    "jira"
]

setup(
    name='robotframework-tesena-tm4j',
    packages=find_packages(),
    version=__version__,
    description='Listener for RobotFramework reporting to SmartBear TM4J plugin',
    author_email='marcel.veselka@tesena.com',
    url='https://gitlab.com/sandbox/listeners/TM4J',
    download_url=(
        'https://gitlab.com/sandbox/listeners/TM4J/'
        'tarball/{version}'.format(version=__version__)),
    keywords=['testing', 'reporting', 'robot framework', 'tm4j'],
    classifiers=[],
    install_requires=requirements
)